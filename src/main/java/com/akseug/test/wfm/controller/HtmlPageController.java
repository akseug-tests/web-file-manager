package com.akseug.test.wfm.controller;

import com.akseug.test.wfm.model.FileNode;
import com.akseug.test.wfm.service.ForbiddenFileServiceException;
import com.akseug.test.wfm.service.IOFileServiceException;
import com.akseug.test.wfm.service.file.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Controller for mapping *.html files
 * <p>
 * <p>
 * Created by axenty on 4/23/17.
 */
@Controller
public class HtmlPageController {

    @RequestMapping(value = "/")
    public String getIndex() {
        return "index.html";
    }

}
