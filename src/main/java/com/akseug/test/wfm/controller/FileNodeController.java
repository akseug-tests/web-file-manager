package com.akseug.test.wfm.controller;

import com.akseug.test.wfm.model.FileNode;
import com.akseug.test.wfm.service.ForbiddenFileServiceException;
import com.akseug.test.wfm.service.IOFileServiceException;
import com.akseug.test.wfm.service.file.FileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Rest Controller for FileNode entities
 * Created by axenty on 4/23/17.
 */
@RestController
@Scope("session")
@Slf4j
public class FileNodeController {

    private final FileService fileService;

    @Autowired
    public FileNodeController(FileService fileService) {
        this.fileService = fileService;
    }

    @PostConstruct
    public void init() {
        log.trace("FileNodeController is created. {} ", this);
    }

    @RequestMapping(value = "/rest/filenode", method = RequestMethod.GET)
    public FileNode getFileNode(@RequestParam(value = "path", required = false) String pathStr,
                                @RequestParam(value = "resolve", required = false) boolean resolve,
                                HttpServletResponse httpServletResponse) throws IOException {
        if (StringUtils.isEmpty(pathStr)) {
            return fileService.getFileNodes();
        } else {
            Path path = Paths.get(pathStr);
            if (resolve) {
                while (true) {
                    try {
                        return fileService.getFileNodes(path);
                    } catch (ForbiddenFileServiceException e) {
                        return fileService.getFileNodes(); //default
                    } catch (IOFileServiceException e) {
                        //maybe alredy remove path or rename, go level up
                        path = path.getParent();
                    }
                }
            } else {
                try {
                    return fileService.getFileNodes(Paths.get(pathStr));
                } catch (ForbiddenFileServiceException e) {
                    httpServletResponse.sendError(HttpServletResponse.SC_FORBIDDEN, "Access is denied!");
                }
                return null;
            }
        }
    }
}
