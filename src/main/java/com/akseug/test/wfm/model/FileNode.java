package com.akseug.test.wfm.model;

import lombok.Data;

import java.util.List;

/**
 * wrapper on java.io.File
 *
 * Created by axenty on 4/23/17.
 */
@Data
public class FileNode {
    String name;
    String path;
    FileNode parent;
    List<FileNode> children;
    long size;
    long createdTimestamp;
    long modifiedTimestamp;
    long lastAccessTimestamp;
    boolean directory;
    String symbolLink;

}
