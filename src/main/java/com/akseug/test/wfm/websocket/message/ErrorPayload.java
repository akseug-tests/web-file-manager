package com.akseug.test.wfm.websocket.message;

import lombok.Data;

/**
 * Error message (both sides)
 *
 * Created by axenty on 4/25/17.
 */
@Data
public class ErrorPayload {
    String msg;

    public ErrorPayload(String msg) {
        this.msg = msg;
    }
}
