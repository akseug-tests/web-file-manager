package com.akseug.test.wfm.websocket.message;

public enum MessageType {
    HELLO_MESSAGE("hello", ClientHelloPayload.class),
    FILE_NODE_MESSAGE("file_node", ServerFilePayload[].class),
    ERROR_MESSAGE("error", ErrorPayload.class);

    private String msgName;
    private Class<?> clazz;

    MessageType(String protocolName, Class<?> clazz) {
        this.msgName = protocolName;
        this.clazz = clazz;
    }

    public static MessageType valueOfByMsg(Message msg) {
        return valueOfByMsg(msg.getMsgName());
    }

    public static MessageType valueOfByMsg(String msgName) {
        switch (msgName) {
            case "hello":
                return HELLO_MESSAGE;
            case "file_node":
                return FILE_NODE_MESSAGE;
        }
        return null;
    }

    public String getMsgName() {
        return msgName;
    }

    public Class<?> getClazz() {
        return clazz;
    }
}
