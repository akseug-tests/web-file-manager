package com.akseug.test.wfm.websocket;

import com.akseug.test.wfm.WebFileManagerException;

/**
 * Common exception for web socket handlers.
 *
 * Created by axenty on 4/25/17.
 */
public class WebSocketException extends WebFileManagerException {
    public WebSocketException() {
    }

    public WebSocketException(String message) {
        super(message);
    }

    public WebSocketException(String message, Throwable cause) {
        super(message, cause);
    }

    public WebSocketException(Throwable cause) {
        super(cause);
    }

    public WebSocketException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
