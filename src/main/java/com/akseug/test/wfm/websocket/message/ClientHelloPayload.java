package com.akseug.test.wfm.websocket.message;

import lombok.Data;

/**
 * Hello message from client
 *
 * Created by axenty on 4/25/17.
 */
@Data
public class ClientHelloPayload {
    private String path;

    public ClientHelloPayload(String path) {
        this.path = path;
    }

    public ClientHelloPayload() {
        //empty for jackson
    }
}
