package com.akseug.test.wfm.websocket.message;

/**
 * File actions
 *
 * Created by axenty on 4/25/17.
 */
public enum Action {
    CREATED,
    REMOVED,
    MODIFIED
}
