package com.akseug.test.wfm.websocket.message;

import lombok.Data;

/**
 * Common message for websocket
 *
 * Created by axenty on 4/25/17.
 */
@Data
public class Message<T> {
    String msgName;
    int statusCode;
    T payload;

    public Message(String msgName, int statusCode, T payload) {
        this.msgName = msgName;
        this.statusCode = statusCode;
        this.payload = payload;
    }

    public Message() {
        //empty for jackson
    }
}
