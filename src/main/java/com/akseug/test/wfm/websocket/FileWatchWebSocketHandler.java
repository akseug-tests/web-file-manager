package com.akseug.test.wfm.websocket;

import com.akseug.test.wfm.service.ForbiddenFileServiceException;
import com.akseug.test.wfm.service.watch.FileWatcher;
import com.akseug.test.wfm.service.watch.WatchFileService;
import com.akseug.test.wfm.websocket.message.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * Web socket handler to file change watcher
 * <p>
 * Created by axenty on 4/25/17.
 */
@Slf4j
class FileWatchWebSocketHandler extends TextWebSocketHandler {

    @Autowired
    private WatchFileService watchFileService;
    @Autowired
    private ObjectMapper objectMapper;

    private HashMap<WebSocketSession, FileWatcherImpl> storage = new HashMap<>();

    @PostConstruct
    public void init() {
        log.trace("FileWatchWebSocketHandler is created! {}", this);
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        log.trace("afterConnectionEstablished session: {}", session);
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        log.trace("handleTextMessage session: {}, message: {}", session, message.getPayload());
        ObjectNode objectNode = objectMapper.readValue(message.getPayload(), ObjectNode.class);
        log.debug("Handle msg: {}.", objectNode);
        String msgName = objectNode.get("msgName").asText();
        if (MessageType.HELLO_MESSAGE.equals(MessageType.valueOfByMsg(msgName))) {
            Message<ClientHelloPayload> helloMessage = objectMapper.readValue(objectMapper.treeAsTokens(objectNode),
                    new TypeReference<Message<ClientHelloPayload>>() {
                    });
            handleHelloMessage(session, helloMessage);
        }
    }

    private void handleHelloMessage(WebSocketSession session, Message<ClientHelloPayload> msg) {
        ClientHelloPayload clientHelloPayload = msg.getPayload();
        String path = clientHelloPayload.getPath();

        if (StringUtils.isEmpty(path)) {
            sendMsg(session, new Message<>(
                    MessageType.ERROR_MESSAGE.getMsgName(),
                    400,
                    new ErrorPayload("Path is empty!")
            ));
            forceClose(session, "Path is empty");
        }

        FileWatcherImpl fileWatcher = storage.get(session);
        if (fileWatcher != null) {
            //already subscribe
            if (fileWatcher.path.equals(path)) {
                //maybe old path equal new path
                log.debug("Ignore client HELLO msg. Reason: already watch for this path: {}.", path);
                return;
            } else {
                //is new path, do unscribe from old paths
                watchFileService.unsubscribe(fileWatcher.path, fileWatcher);
            }
        }
        try {
            fileWatcher = new FileWatcherImpl(session, path); //create new
            storage.put(session, fileWatcher);
            watchFileService.subscribe(path, fileWatcher);
        } catch (ForbiddenFileServiceException e) {
            log.trace(e.getMessage(), e);
            sendMsg(session, new Message<>(
                    MessageType.ERROR_MESSAGE.getMsgName(),
                    403,
                    new ErrorPayload("Forbidden path!")
            ));
            forceClose(session, "Forbidden path!");
        }
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        log.trace("afterConnectionClosed session: {}, status: {}", session, status);
        FileWatcherImpl fileWatcher = storage.get(session);
        if (fileWatcher != null) {
            watchFileService.unsubscribe(fileWatcher.path, fileWatcher);
        }
    }

    private void sendMsg(WebSocketSession session, Message message) {
        try {
            String json = objectMapper.writeValueAsString(message);
            log.debug("Send message: {}.", json);
            session.sendMessage(new TextMessage(json));
        } catch (IOException e) {
            throw new WebSocketException(e.getMessage(), e);
        }
    }

    private void forceClose(WebSocketSession session, String reason) {
        log.info("Session {} will closed, reason: {}", session.getId(), reason);
        try {
            session.close();
        } catch (IOException e) {
            throw new WebSocketException(e.getMessage(), e);
        }
    }

    private class FileWatcherImpl implements FileWatcher {

        WebSocketSession session;
        String path;

        FileWatcherImpl(WebSocketSession session, String path) {
            this.session = session;
            this.path = path;
        }

        @Override
        public void notify(List<ServerFilePayload> payloadList) {
            ServerFilePayload[] serverFilePayloads = payloadList.toArray(new ServerFilePayload[0]);
            sendMsg(session, new Message<>(MessageType.FILE_NODE_MESSAGE.getMsgName(), 200, serverFilePayloads));
        }
    }
}
