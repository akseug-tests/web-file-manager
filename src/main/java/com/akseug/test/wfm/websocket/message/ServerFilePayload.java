package com.akseug.test.wfm.websocket.message;

import com.akseug.test.wfm.model.FileNode;
import lombok.Data;

/**
 * Message from server with files changes
 *
 * Created by axenty on 4/25/17.
 */
@Data
public class ServerFilePayload {
    private Action action;
    private FileNode fileNode;
}
