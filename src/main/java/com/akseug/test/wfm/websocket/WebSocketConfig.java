package com.akseug.test.wfm.websocket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/**
 * Websocket config and mapping
 *
 * Created by axenty on 4/25/17.
 */
@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(getFileWatchWebSocketHandler(), "/sjs/filewatch").withSockJS();
    }

    @Bean
    public WebSocketHandler getFileWatchWebSocketHandler() {
        return new FileWatchWebSocketHandler();
    }

}
