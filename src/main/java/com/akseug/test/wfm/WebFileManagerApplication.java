package com.akseug.test.wfm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class WebFileManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebFileManagerApplication.class, args);
    }
}
