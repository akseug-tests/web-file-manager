package com.akseug.test.wfm;

/**
 * Warning! Is extends RuntimeException! Maybe is very very bad idea, but I am happy now (no throws and try-catches).
 * All my exceptions will extends from it. If need future, then will replaced to Exception and will refactor code.
 *
 * Created by axenty on 4/23/17.
 */
public class WebFileManagerException extends RuntimeException {
    public WebFileManagerException() {
    }

    public WebFileManagerException(String message) {
        super(message);
    }

    public WebFileManagerException(String message, Throwable cause) {
        super(message, cause);
    }

    public WebFileManagerException(Throwable cause) {
        super(cause);
    }

    public WebFileManagerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
