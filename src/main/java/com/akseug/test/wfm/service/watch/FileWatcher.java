package com.akseug.test.wfm.service.watch;

import com.akseug.test.wfm.websocket.message.ServerFilePayload;

import java.util.List;

/**
 * Interface FileWatcher for notify of files changes from WatchFileService
 *
 * Created by axenty on 4/25/17.
 */
public interface FileWatcher {

    void notify(List<ServerFilePayload> fileNodes);

}
