package com.akseug.test.wfm.service;

import com.akseug.test.wfm.WebFileManagerException;

/**
 * Created by axenty on 4/25/17.
 */
public class ForbiddenFileServiceException extends WebFileManagerException {
    public ForbiddenFileServiceException() {
    }

    public ForbiddenFileServiceException(String message) {
        super(message);
    }

    public ForbiddenFileServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ForbiddenFileServiceException(Throwable cause) {
        super(cause);
    }

    public ForbiddenFileServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
