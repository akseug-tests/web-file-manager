package com.akseug.test.wfm.service.watch;

import com.akseug.test.wfm.model.FileNode;
import com.akseug.test.wfm.service.ForbiddenFileServiceException;
import com.akseug.test.wfm.service.IOFileServiceException;
import com.akseug.test.wfm.service.file.FileService;
import com.akseug.test.wfm.websocket.message.Action;
import com.akseug.test.wfm.websocket.message.ServerFilePayload;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.*;
import java.util.*;

/**
 * Implementation of service watcher for files changes
 * <p>
 * Created by axenty on 4/25/17.
 */
@Service
@Slf4j
public class WatchFileServiceImpl implements WatchFileService {

    private final Map<Path, Set<FileWatcher>> fileWatcherMap = new HashMap<>();
    private final Map<Path, WatchService> watchServiceMap = new HashMap<>();
    private final LinkedList<SendTask> sendTasks = new LinkedList<>();

    private final FileService fileService;

    @Autowired
    public WatchFileServiceImpl(FileService fileService) {
        this.fileService = fileService;
    }

    @PostConstruct
    public void init() {
        log.trace("{} is created!", this);
    }

    @Override
    public void subscribe(String pathStr, FileWatcher fileWatcher) throws ForbiddenFileServiceException {
        Path path = Paths.get(pathStr);
        if (fileService.isChild(path)) {
            fileWatcherMap
                    .computeIfAbsent(path, p -> new HashSet<>())
                    .add(fileWatcher);
        } else {
            throw new ForbiddenFileServiceException(String.format("Forbidden file by path: %s.", pathStr));
        }
    }

    @Override
    public void unsubscribe(String pathStr, FileWatcher fileWatcher) {
        Path path = Paths.get(pathStr);
        Set<FileWatcher> fileWatchers = fileWatcherMap.get(path);
        if (fileWatchers != null) {
            fileWatchers.remove(fileWatcher);
            if (fileWatchers.size() == 0) {
                fileWatcherMap.remove(path);

                //close watchService
                WatchService watchService = getWatchService(path);
                watchServiceMap.remove(path);
                try {
                    watchService.close();
                } catch (IOException e) {
                    throw new IOFileServiceException(e.getMessage(), e);
                }
            }
        }
    }

    private WatchService getWatchService(Path path) {
        return watchServiceMap.computeIfAbsent(path, p -> {
            WatchService watchService;
            try {
                watchService = path.getFileSystem().newWatchService();
                path.register(watchService,
                        StandardWatchEventKinds.ENTRY_CREATE,
                        StandardWatchEventKinds.ENTRY_DELETE,
                        StandardWatchEventKinds.ENTRY_MODIFY);
            } catch (IOException e) {
                throw new IOFileServiceException(e.getMessage(), e);
            }
            return watchService;
        });
    }

    @Scheduled(fixedDelay = 100)
    private void handleSendTask() {
        SendTask sendTask = sendTasks.poll();
        if (sendTask != null) {
            log.debug("Handle {} ", sendTask);
            //convert to ServerFilePayload list
            Map<Path, String> fileEvents = sendTask.getFileEvents();
            List<ServerFilePayload> fileNodeMessages = new ArrayList<>(fileEvents.size());
            for (Map.Entry<Path, String> entry : fileEvents.entrySet()) {
                try {
                    fileNodeMessages.add(convert(entry));
                } catch (IOFileServiceException ex) {
                    //if file was created and I handle this now, then there is a chance file already removed from FS
                    //only log for this situation
                    log.info(ex.getMessage(), ex);
                } catch (ForbiddenFileServiceException ex) {
                    log.error(ex.getMessage(), ex); //Something went wrong, this is not a possible
                }
            }

            //send to ServerFilePayload list to fileWatchers
            Set<FileWatcher> fileWatchers = fileWatcherMap.get(sendTask.getPath());
            if (fileWatchers != null) {
                log.debug("For path {} found {} watchers.", sendTask.getPath().toString(), fileWatchers.size());
                for (FileWatcher fileWatcher : fileWatchers) {
                    try {
                        fileWatcher.notify(fileNodeMessages);
                    } catch (Exception e) {
                        log.warn(e.getMessage(), e); //can not crash if handler throw exception
                    }
                }
            }
        }
    }

    private ServerFilePayload convert(Map.Entry<Path, String> entry) {
        ServerFilePayload fileNodeMessage = new ServerFilePayload();
        switch (entry.getValue()) {
            case "ENTRY_CREATE":
                fileNodeMessage.setAction(Action.CREATED);
                break;
            case "ENTRY_MODIFY":
                fileNodeMessage.setAction(Action.MODIFIED);
                break;
            case "ENTRY_DELETE":
                fileNodeMessage.setAction(Action.REMOVED);
                break;
        }
        FileNode fileNode;
        if (Action.REMOVED.equals(fileNodeMessage.getAction())) {
            fileNode = new FileNode();
            fileNode.setName(entry.getKey().getFileName().toString());
            fileNode.setPath(entry.getKey().toString()); //set path if removed
        } else {
            fileNode = fileService.getFileNode(entry.getKey()); //notify file parameters
        }
        fileNodeMessage.setFileNode(fileNode);
        return fileNodeMessage;
    }

    @Scheduled(fixedDelay = 100)
    public void checkFileEvents() {
        Set<Path> paths = fileWatcherMap.keySet();
        for (Path path : paths) {
            WatchService watchService = getWatchService(path);
            WatchKey key;
            key = watchService.poll();
            if (key != null) {
                //file system has events
                Map<Path, String> fileEvents = new HashMap<>();
                for (WatchEvent event : key.pollEvents()) {
                    switch (event.kind().name()) {
                        case "ENTRY_CREATE":
                        case "ENTRY_MODIFY":
                        case "ENTRY_DELETE":
                            Path childPath = Paths.get(path.toString(), event.context().toString());
                            fileEvents.put(childPath, event.kind().name());
                            log.debug("File: {} Event: {}", childPath, event.kind().name());
                            break;
                    }
                }
                SendTask sendTask = new SendTask();
                sendTask.setFileEvents(fileEvents);
                sendTask.setPath(path);
                sendTasks.add(sendTask);
                key.reset();
            }
        }
    }

    @Data
    private class SendTask {
        Path path;
        Map<Path, String> fileEvents;
    }

}
