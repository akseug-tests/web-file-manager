package com.akseug.test.wfm.service.watch;

import org.springframework.stereotype.Service;

/**
 * Service watcher for files changes
 *
 * Created by axenty on 4/25/17.
 */
public interface WatchFileService {
    void subscribe(String path, FileWatcher fileWatcher);
    void unsubscribe(String path, FileWatcher fileWatcher);
}
