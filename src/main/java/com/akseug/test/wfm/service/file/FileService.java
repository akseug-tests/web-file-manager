package com.akseug.test.wfm.service.file;

import com.akseug.test.wfm.model.FileNode;

import java.nio.file.Path;

/**
 * Created by axenty on 4/23/17.
 */
public interface FileService {
    FileNode getFileNodes();

    FileNode getFileNodes(Path path);

    FileNode getFileNode(Path path);

    boolean isChild(Path path);
}
