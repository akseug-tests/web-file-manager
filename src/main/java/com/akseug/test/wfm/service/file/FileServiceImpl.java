package com.akseug.test.wfm.service.file;

import com.akseug.test.wfm.model.FileNode;
import com.akseug.test.wfm.service.ForbiddenFileServiceException;
import com.akseug.test.wfm.service.IOFileServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

/**
 * Service for work with filesystem
 * <p>
 * Created by axenty on 4/23/17.
 */
@Service
//@Scope(value = "session")
@Slf4j
public class FileServiceImpl implements FileService {

    private String defaultPathStr;

    public FileServiceImpl(@Value("${wfm.defaultPath}") String defaultPathStr) {
        this.defaultPathStr = defaultPathStr;
    }

    /**
     * @return tree nodes for default folder
     */
    @Override
    public FileNode getFileNodes() {
        log.trace("Get FileNode for default path: {}", defaultPathStr);
        Path defaultPath = Paths.get(defaultPathStr);
        return convert(toRealPath(defaultPath), true, true);
    }

    /**
     * @param path filesystem path
     * @return path tree nodes for path (with parent and children)
     * @throws ForbiddenFileServiceException
     */
    @Override
    public FileNode getFileNodes(Path path) throws ForbiddenFileServiceException, IOFileServiceException {
        log.trace("Get FileNodes for path: {}", path);
        if (isChild(path)) {
            return convert(toRealPath(path), true, true);
        } else {
            log.warn("Access is denied to path: {}", path.toString());
            throw new ForbiddenFileServiceException(String.format("Forbidden file by path: %s.", path.toString()));
        }
    }

    /**
     * @param path filesystem path
     * @return one node by path (without parent and children)
     * @throws ForbiddenFileServiceException
     */
    @Override
    public FileNode getFileNode(Path path) throws ForbiddenFileServiceException {
        log.trace("Get FileNode for path: {}", path);
        if (isChild(path)) {
            return convert(toRealPath(path), false, false);
        } else {
            log.warn("Access is denied to path: {}", path.toString());
            throw new ForbiddenFileServiceException(String.format("Forbidden file by path: %s.", path.toString()));
        }
    }

    private Path toRealPath(Path path) {
        try {
            if (Files.isSymbolicLink(path)) {
                Path parentRealPath = path.getParent().toRealPath();
                return Paths.get(parentRealPath.toString(), path.getFileName().toString());
            } else {
                return path.toRealPath();
            }
        } catch (IOException e) {
            throw new IOFileServiceException(e.getMessage(), e);
        }
    }

    /**
     * @param children path
     * @return true if is children for default path
     * @throws IOFileServiceException if file isn't exist
     */
    @Override
    public boolean isChild(Path children) throws IOFileServiceException {
        Path parent = Paths.get(defaultPathStr);
        return toRealPath(children).startsWith(toRealPath(parent));
    }

    private void fillParent(Path path, FileNode fileNode) {
        Path parent = path.getParent();
        if (parent != null) {
            fileNode.setParent(convert(parent, false, false));
        }
    }

    private void fillChildren(Path path, FileNode fileNode) {
        if (Files.isDirectory(path)) {
            try {
                List<FileNode> childrenList = new ArrayList<>();
                Files.list(path).forEach(childrenPath -> childrenList.add(convert(childrenPath, false, false)));
                fileNode.setChildren(childrenList);
            } catch (IOException e) {
                throw new IOFileServiceException(e.getMessage(), e);
            }
        }
    }

    private FileNode convert(Path path, boolean fillParent, boolean fillChildren) {
        FileNode resultNode = new FileNode();
        resultNode.setName(path.getFileName() == null ? "" : path.getFileName().toString());
        resultNode.setPath(path.toString());
        if (Files.isSymbolicLink(path)) {
            try {
                resultNode.setSymbolLink(Files.readSymbolicLink(path).toString());
            } catch (IOException e) {
                throw new IOFileServiceException(e.getMessage(), e);
            }
        } else {
            BasicFileAttributes attributes;
            try {
                attributes = Files.readAttributes(path, BasicFileAttributes.class);
            } catch (IOException e) {
                throw new IOFileServiceException(e.getMessage(), e);
            }

            resultNode.setCreatedTimestamp(attributes.creationTime().toMillis());
            resultNode.setModifiedTimestamp(attributes.lastModifiedTime().toMillis());
            resultNode.setLastAccessTimestamp(attributes.lastAccessTime().toMillis());
            resultNode.setSize(attributes.size());
            resultNode.setDirectory(attributes.isDirectory());
        }
        if (fillParent) {
            fillParent(path, resultNode);
        }
        if (fillChildren) {
            fillChildren(path, resultNode);
        }
        return resultNode;
    }

}
