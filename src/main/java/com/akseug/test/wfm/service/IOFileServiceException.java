package com.akseug.test.wfm.service;

import com.akseug.test.wfm.WebFileManagerException;

/**
 * Wrap IOExceptions
 *
 * Created by axenty on 4/23/17.
 */
public class IOFileServiceException extends WebFileManagerException {
    public IOFileServiceException() {
    }

    public IOFileServiceException(String message) {
        super(message);
    }

    public IOFileServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public IOFileServiceException(Throwable cause) {
        super(cause);
    }

    public IOFileServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
