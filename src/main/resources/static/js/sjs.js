/*
 * websocket logic
 */
var sock = null;
var lastPath = null;

function connectWebSocket(path) {
    sock = new SockJS('/sjs/filewatch');
    sock.onmessage = function (e) {
        console.log('message', e.data);
        handleMessage(e.data);
    }
    sock.onopen = function () {
        console.log('open');
        sock.isOpen = true;
        sendMessage(getHelloMessage(path));
    }
    sock.onclose = function (e) {
        console.log("onclose");
        disconnectWebSocket();
        setTimeout(function () {
            connectWebSocket(lastPath)
        }, 3000)
    }
}

function disconnectWebSocket() {
    if (sock.isOpen == true) {
        sock.close();
    }
    console.log("Disconnected");
}

function getHelloMessage(path) {
    lastPath = path;
    return JSON.stringify({
        msgName: "hello",
        statusCode: 0,
        payload: {
            path: path
        }
    });
}

function sendMessage(msg) {
    if (sock.isOpen == true) {
        sock.send(msg);
        console.log("send ", msg)
    } else {
        console.log("try send (sockjs isn't ready yet)", msg)
    }
}

function handleMessage(data) {
    var msg = JSON.parse(data);
    if (msg != null) {
        if ('file_node' == msg.msgName && 200 == msg.statusCode) {
            var fileNodeActions = msg.payload;
            fileNodeActions.forEach(function (fileNodeAction, i, arr) {
                var action = fileNodeAction.action;
                var fileNode = fileNodeAction.fileNode;
                var $tr = $("tr[path='" + fileNode.path + "']");

                if ($tr.hasClass("file-node-parent")) {
                    //is parent, change name and priority
                    convertParent(fileNode);
                } else if ($tr.hasClass("file-node-current")) {
                    //is current, change name and priority
                    convertCurrent(fileNode)
                } else {
                    //is child
                    convertChild(fileNode);
                }
                if (action === "REMOVED") {
                    dataTable.row($tr).remove().draw(false);
                    toastr["warning"]("File name: " + fileNode.name + "<br/> Path: " + fileNode.path, "Remove")
                } else if (action === "MODIFIED" || action === "CREATED") {
                    if ($tr.length > 0) {
                        dataTable.row($tr).remove().row.add(fileNode).draw(false);
                        //is update! object already in table
                        toastr["info"]("File name: " + fileNode.name + "<br/> Path: " + fileNode.path, "Update")
                    } else {
                        dataTable.row.add(fileNode).draw(false);
                        toastr["success"]("File name: " + fileNode.name + "<br/> Path: " + fileNode.path, "Create")
                    }
                }
            });
        }
    }
}