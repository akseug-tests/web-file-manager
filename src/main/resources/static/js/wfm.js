/*
 * Datatable logic and convert data
 */
var dataTable = null;

function applyDataTable() {
    var $tableWfm = $('#table-web-file-manage');
    dataTable = $tableWfm.DataTable({
        columns: [
            {title: "Priority", data: "priority", visible: false, className: "file-node-priority"},
            {title: "Type", data: "type", className: "file-node-type img-common-icon", createdCell: drawIcon},
            {title: "Name", data: "name", className: "file-node-name", createdCell: concatNameAndLink},
            {title: "Size", data: "size", className: "file-node-size"},
            {title: "Created", data: "createdTimestamp", render: timestampToDate, className: "file-node-created"},
            {title: "Access date", data: "lastAccessTimestamp", render: timestampToDate, className: "file-node-access"},
            {title: "Modified", data: "modifiedTimestamp", render: timestampToDate, className: "file-node-modified"}
        ],
        order: [[0, 'asc'], [1, 'asc'], [2, 'asc']],
        paging: false,
        scrollY: $(document).height() - 200,
        keys: true,
        createdRow: function (row, data, dataIndex) {
            $(row).attr("path", data.path);
            if (data.role == 'parent') {
                $(row).addClass('file-node-parent');
            } else if (data.role == 'current') {
                $(row).addClass('file-node-current');
            } else {
                $(row).addClass('file-node-children');
            }
        },
        destroy: true
    });

    dataTable.order.fixed({
        pre: [0, 'asc']
    });

    dataTable.on('key-focus', function (e, datatable, cell) {
        if ($(cell.node()).parent().hasClass('file-node-current')) {
            selectParent();
        } else {
            datatable.rows().deselect();
            datatable.row(cell.index().row).select();
            $(datatable.row(cell.index().row).node()).children().removeClass("focus")
        }
    });

    dataTable.on('key', function (e, datatable, key, cell, originalEvent) {
        if (key === 13) { // return
            var data = datatable.row(cell.index().row).data();
            if (data.directory == true && data.role != "current") {
                path = data.path;
                changePath(path);
            }
        }
    });

    dataTable.on('user-select', function (e, dt, type, cell, originalEvent) {
        if (originalEvent.target.nodeName.toLowerCase() === 'img') {
            e.preventDefault();
        }
    });
    $tableWfm.on('dblclick', 'tr', function () {
        var data = dataTable.row(this).data();
        if (data.directory == true && data.role != "current") {
            path = data.path;
            changePath(path);
        }
    });
    $tableWfm.on('order.dt', function () {
        //updateHeaderNodes();
    });

}

function loadFileNodes(path) {
    var url = "/rest/filenode";
    if (path != null) {
        url = url + "?resolve=true&path=" + path;
    }
    $.ajax({
        url: url,
        context: document.body
    }).done(function (current) {
        console.log("done ", current);
        if (sock != null && sock.isOpen == true) {
            sendMessage(getHelloMessage(current.path));
        } else {
            connectWebSocket(current.path); //subscribe
        }

        var children = current.children;
        var parent = current.parent;

        convertParent(parent);
        convertCurrent(current);
        convertChildren(children);

        var arrayData = [current, parent].concat(children);
        dataTable.clear().rows.add(arrayData).draw();
        selectParent();
    });
}

function timestampToDate(data, type, row) {
    return moment(data).format("DD-MMM-YYYY HH:mm:ss");
}

function drawIcon(td, cellData, rowData, row, col) {
    if (cellData == 'D') {
        $(td).addClass('img-folder');
    } else if (cellData == 'L') {
        $(td).addClass('img-link');
    } else {
        $(td).addClass('img-file');
    }
}

function concatNameAndLink(td, cellData, rowData, row, col) {
    if (rowData.symbolLink != null) {
        $(td).text(rowData.name + " --> " + rowData.symbolLink);
    } else {
        return $(td).text(rowData.name);
    }
}

function selectParent() {
    var parentElements = $(".file-node-parent").children();
    if (parentElements.length > 0) {
        parentElements[0].click();
    }
}

function changePath(path) {
    loadFileNodes(path);
    dataTable.search("").draw();
}

function setType(data) {
    if (data.directory) {
        data.type = 'D';
    } else if (data.symbolLink != null) {
        data.type = 'L';
    } else {
        data.type = 'F';
    }
    return data;
}
function convertParent(parent) {
    setType(parent);
    parent.name = "..";
    parent.role = 'parent';
    parent.priority = 1;
    return parent;
}
function convertCurrent(current) {
    setType(current);
    current.name = current.path;
    current.role = 'current';
    current.priority = 0;
    return current;
}
function convertChild(child) {
    setType(child);
    child.priority = 999;
    return child;
}
function convertChildren(array) {
    array.forEach(function (child) {
        convertChild(child);
    })
}